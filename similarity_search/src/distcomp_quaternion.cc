/**
 * Non-metric Space Library
 *
 * Authors: Bilegsaikhan Naidan (https://github.com/bileg), Leonid Boytsov (http://boytsov.info).
 * With contributions from Lawrence Cayton (http://lcayton.com/) and others.
 *
 * For the complete list of contributors and further details see:
 * https://github.com/searchivarius/NonMetricSpaceLib
 *
 * Copyright (c) 2014
 *
 * This code is released under the
 * Apache License Version 2.0 http://www.apache.org/licenses/.
 *
 */
#include "distcomp.h"
#include "string.h"
#include "logging.h"
#include "utils.h"
#include "pow.h"
#include "portable_intrinsics.h"

#include <cstdlib>
#include <limits>
#include <algorithm>
#include <cmath>

namespace similarity {

using namespace std;

/*
 * Quaternion1-distance.
 */

template <class T>
T Quaternion1Standard(const T *p1, const T *p2, size_t qty)
{
    T sum = 0;

    for (size_t i = 0; i < qty; i++) {
      T diff = (p1[i]-p2[i]);
      sum += diff * diff;
    }

    if (sum < 2.0)
        return sqrt(sum);
    else
        return sqrt(4.0 - sum);
}

template  float Quaternion1Standard<float>(const float *p1, const float *p2, size_t qty);
template  double Quaternion1Standard<double>(const double *p1, const double *p2, size_t qty);


template <class T>
T Quaternion1(const T* pVect1, const T* pVect2, size_t qty) {
    //Probably faster, assumes qty = 4.
    T res = 0, diff = 0;

    diff = *pVect1++ - *pVect2++; res += diff * diff;
    diff = *pVect1++ - *pVect2++; res += diff * diff;
    diff = *pVect1++ - *pVect2++; res += diff * diff;
    diff = *pVect1++ - *pVect2++; res += diff * diff;

    if (res < 2.0)
        return sqrt(res);
    else
        return sqrt(4.0 - res);
}

template float  Quaternion1<float>(const float* pVect1, const float* pVect2, size_t qty);
template double Quaternion1<double>(const double* pVect1, const double* pVect2, size_t qty);

/*
 * On new architectures unaligned loads are almost as fast as aligned ones.
 * Ensuring that both pVect1 and pVect2 are similarly aligned could be hard.
 */

template <>
float Quaternion1SIMD(const float* pVect1, const float* pVect2, size_t qty) {
#ifndef PORTABLE_SSE2
#pragma message WARN("Quaternion1SIMD<float>: SSE2 is not available, defaulting to pure C++ implementation!")
    return Quaternion1Standard(pVect1, pVect2, qty);
#else
    const __m128 v1         = _mm_loadu_ps(pVect1);                              // [ v1D v1C | v1B v1A ]
    const __m128 v2         = _mm_loadu_ps(pVect2);                              // [ v2D v2C | v2B v2A ]
    const __m128 diff1      = _mm_sub_ps(v1, v2);                                // [ v1D-v2D v1C-v2C | v1B-v2B v1A-v2B ]
    const __m128 sqr1       = _mm_mul_ps(diff1, diff1);                          // [ (v1D-v2D)**2 (v1C-v2C)**2 | (v1B-v2B)**2 (v1A-v2A)**2 ]

    // horizontal sum
    const __m128 shuffle1   = _mm_shuffle_ps(sqr1, sqr1, _MM_SHUFFLE(1,0,3,2));
	const __m128 sum1       = _mm_add_ps(sqr1, shuffle1);
	const __m128 shuffle2   = _mm_shuffle_ps(sum1, sum1, _MM_SHUFFLE(2,3,0,1));
	const __m128 sum2       = _mm_add_ps(sum1, shuffle2);

    // carry out minimum
    const __m128 fours      = _mm_set_ss(4.0);
    const __m128 mins       = _mm_min_ss(sum2, _mm_sub_ps(fours, sum2));
    const __m128 res        = _mm_sqrt_ss(mins);
    float result;
    _mm_store_ss(&result, res);
    return result;
#endif
}

template <>
double Quaternion1SIMD(const double* pVect1, const double* pVect2, size_t qty) {
    return Quaternion1Standard(pVect1, pVect2, qty);
}

template float  Quaternion1SIMD<float>(const float* pVect1, const float* pVect2, size_t qty);
template double Quaternion1SIMD<double>(const double* pVect1, const double* pVect2, size_t qty);


/*
 * Quaternion2-distance.
 */

template <class T>
T Quaternion2Standard(const T *p1, const T *p2, size_t qty)
{
    T sum1 = 0;
    T sum2 = 0;

    for (size_t i = 0; i < 4; i++) {
      T diff1 = (p1[i]-p2[i]);
      sum1 += diff1 * diff1;

      T diff2 = (p1[i+4]-p2[i+4]);
      sum2 += diff2 * diff2;
    }

    if (sum1 > 2.0)
        sum1 = 4.0 - sum1;
    if (sum2 > 2.0)
        sum2 = 4.0 - sum2;

    return sqrt(sum1 + sum2);
}

template  float Quaternion2Standard<float>(const float *p1, const float *p2, size_t qty);
template  double Quaternion2Standard<double>(const double *p1, const double *p2, size_t qty);


template <class T>
T Quaternion2(const T* pVect1, const T* pVect2, size_t qty) {

    // Probably faster. Assumes qty = 8.
    T res1 = 0, res2 = 0, diff = 0;

    diff = *pVect1++ - *pVect2++; res1 += diff * diff;
    diff = *pVect1++ - *pVect2++; res1 += diff * diff;
    diff = *pVect1++ - *pVect2++; res1 += diff * diff;
    diff = *pVect1++ - *pVect2++; res1 += diff * diff;

    diff = *pVect1++ - *pVect2++; res2 += diff * diff;
    diff = *pVect1++ - *pVect2++; res2 += diff * diff;
    diff = *pVect1++ - *pVect2++; res2 += diff * diff;
    diff = *pVect1++ - *pVect2++; res2 += diff * diff;


    if (res1 > 2.0)
        res1 = 4.0 - res1;
    if (res2 > 2.0)
        res2 = 4.0 - res2;

    return sqrt(res1 + res2);
}

template float  Quaternion2<float>(const float* pVect1, const float* pVect2, size_t qty);
template double Quaternion2<double>(const double* pVect1, const double* pVect2, size_t qty);


/*
 * On new architectures unaligned loads are almost as fast as aligned ones.
 * Ensuring that both pVect1 and pVect2 are similarly aligned could be hard.
 */

template <>
float Quaternion2SIMD(const float* pVect1, const float* pVect2, size_t qty) {
#ifndef PORTABLE_SSE2
#pragma message WARN("Quaternion2SIMD<float>: SSE2 is not available, defaulting to pure C++ implementation!")
    return Quaternion2Standard(pVect1, pVect2, qty);

#else
    const __m128 v1       = _mm_loadu_ps(pVect1); pVect1 += 4;
    const __m128 v2       = _mm_loadu_ps(pVect2); pVect2 += 4;
    const __m128 diff1    = _mm_sub_ps(v1, v2);
    const __m128 sqr1     = _mm_mul_ps(diff1, diff1);
    const __m128 shuffle1 = _mm_shuffle_ps(sqr1, sqr1, _MM_SHUFFLE(1,0,3,2));
	const __m128 sum1     = _mm_add_ps(sqr1, shuffle1);
	const __m128 shuffle2 = _mm_shuffle_ps(sum1, sum1, _MM_SHUFFLE(2,3,0,1));
	const __m128 sum2     = _mm_add_ps(sum1, shuffle2);

    const __m128 v3       = _mm_loadu_ps(pVect1); //pVect1 += 4;
    const __m128 v4       = _mm_loadu_ps(pVect2); //pVect2 += 4;
    const __m128 diff3    = _mm_sub_ps(v3, v4);
    const __m128 sqr3     = _mm_mul_ps(diff3, diff3);
    const __m128 shuffle3 = _mm_shuffle_ps(sqr3, sqr3, _MM_SHUFFLE(1,0,3,2));
	const __m128 sum3     = _mm_add_ps(sqr3, shuffle3);
	const __m128 shuffle4 = _mm_shuffle_ps(sum3, sum3, _MM_SHUFFLE(2,3,0,1));
	const __m128 sum4     = _mm_add_ps(sum3, shuffle4);

    const __m128 fours    = _mm_set1_ps(4.0);
    const __m128 comb     = _mm_unpacklo_ps(sum2, sum4);
    const __m128 mins     = _mm_min_ps(comb, _mm_sub_ps(fours, comb));
    const __m128 mins_shuf= _mm_shuffle_ps(mins, mins, _MM_SHUFFLE(0,0,0,1));
    const __m128 res      = _mm_sqrt_ss(_mm_add_ss(mins, mins_shuf));
    float result;
    _mm_store_ss(&result, res);
    return result;
#endif
}

template <>
double Quaternion2SIMD(const double* pVect1, const double* pVect2, size_t qty) {
    return Quaternion2Standard(pVect1, pVect2, qty);
}

template float  Quaternion2SIMD<float>(const float* pVect1, const float* pVect2, size_t qty);
template double Quaternion2SIMD<double>(const double* pVect1, const double* pVect2, size_t qty);


/*
 * Quaternion3-distance.
 */

template <class T>
T Quaternion3Standard(const T *p1, const T *p2, size_t qty)
{
    T sum1 = 0;
    T sum2 = 0;
    T sum3 = 0;

    for (size_t i = 0; i < 4; i++) {
      T diff1 = (p1[i]-p2[i]);
      sum1 += diff1 * diff1;

      T diff2 = (p1[i+4]-p2[i+4]);
      sum2 += diff2 * diff2;

      T diff3 = (p1[i+8]-p2[i+8]);
      sum3 += diff3 * diff3;
    }

    if (sum1 > 2.0)
        sum1 = 4.0 - sum1;
    if (sum2 > 2.0)
        sum2 = 4.0 - sum2;
    if (sum3 > 2.0)
        sum3 = 4.0 - sum3;

    return sqrt(sum1 + sum2 + sum3);
}

template  float Quaternion3Standard<float>(const float *p1, const float *p2, size_t qty);
template  double Quaternion3Standard<double>(const double *p1, const double *p2, size_t qty);


template <class T>
T Quaternion3(const T* pVect1, const T* pVect2, size_t qty) {

    T res1 = 0, res2 = 0, res3 = 0, diff = 0;

    diff = *pVect1++ - *pVect2++; res1 += diff * diff;
    diff = *pVect1++ - *pVect2++; res1 += diff * diff;
    diff = *pVect1++ - *pVect2++; res1 += diff * diff;
    diff = *pVect1++ - *pVect2++; res1 += diff * diff;

    diff = *pVect1++ - *pVect2++; res2 += diff * diff;
    diff = *pVect1++ - *pVect2++; res2 += diff * diff;
    diff = *pVect1++ - *pVect2++; res2 += diff * diff;
    diff = *pVect1++ - *pVect2++; res2 += diff * diff;

    diff = *pVect1++ - *pVect2++; res3 += diff * diff;
    diff = *pVect1++ - *pVect2++; res3 += diff * diff;
    diff = *pVect1++ - *pVect2++; res3 += diff * diff;
    diff = *pVect1++ - *pVect2++; res3 += diff * diff;


    if (res1 > 2.0)
        res1 = 4.0 - res1;
    if (res2 > 2.0)
        res2 = 4.0 - res2;
    if (res3 > 2.0)
        res3 = 4.0 - res3;

    return sqrt(res1 + res2 + res3);
}

template float  Quaternion3<float>(const float* pVect1, const float* pVect2, size_t qty);
template double Quaternion3<double>(const double* pVect1, const double* pVect2, size_t qty);


template <>
float Quaternion3SIMD(const float* pVect1, const float* pVect2, size_t qty) {
#ifndef PORTABLE_SSE2
#pragma message WARN("Quaternion3SIMD<float>: SSE2 is not available, defaulting to pure C++ implementation!")
    return Quaternion3Standard(pVect1, pVect2, qty);

#else
    const __m128 v1       = _mm_loadu_ps(pVect1); pVect1 += 4;
    const __m128 v2       = _mm_loadu_ps(pVect2); pVect2 += 4;
    const __m128 diff1    = _mm_sub_ps(v1, v2);
    const __m128 sqr1     = _mm_mul_ps(diff1, diff1);
    const __m128 shuffle1 = _mm_shuffle_ps(sqr1, sqr1, _MM_SHUFFLE(1,0,3,2));
	const __m128 sum1     = _mm_add_ps(sqr1, shuffle1);
	const __m128 shuffle2 = _mm_shuffle_ps(sum1, sum1, _MM_SHUFFLE(2,3,0,1));
	const __m128 sum2     = _mm_add_ps(sum1, shuffle2);

    const __m128 v3       = _mm_loadu_ps(pVect1); pVect1 += 4;
    const __m128 v4       = _mm_loadu_ps(pVect2); pVect2 += 4;
    const __m128 diff3    = _mm_sub_ps(v3, v4);
    const __m128 sqr3     = _mm_mul_ps(diff3, diff3);
    const __m128 shuffle3 = _mm_shuffle_ps(sqr3, sqr3, _MM_SHUFFLE(1,0,3,2));
	const __m128 sum3     = _mm_add_ps(sqr3, shuffle3);
	const __m128 shuffle4 = _mm_shuffle_ps(sum3, sum3, _MM_SHUFFLE(2,3,0,1));
	const __m128 sum4     = _mm_add_ps(sum3, shuffle4);

    const __m128 v5       = _mm_loadu_ps(pVect1); // pVect1 += 4;
    const __m128 v6       = _mm_loadu_ps(pVect2); // pVect2 += 4;
    const __m128 diff5    = _mm_sub_ps(v5, v6);
    const __m128 sqr5     = _mm_mul_ps(diff5, diff5);
    const __m128 shuffle5 = _mm_shuffle_ps(sqr5, sqr5, _MM_SHUFFLE(1,0,3,2));
	const __m128 sum5     = _mm_add_ps(sqr5, shuffle5);
	const __m128 shuffle6 = _mm_shuffle_ps(sum5, sum5, _MM_SHUFFLE(2,3,0,1));
	const __m128 sum6     = _mm_add_ps(sum5, shuffle6);

    const __m128 fours    = _mm_set1_ps(4.0);
    const __m128 comb     = _mm_unpacklo_ps(sum2, sum4);
    const __m128 comb2    = _mm_movehl_ps(comb, sum6);
    const __m128 mins     = _mm_min_ps(comb2, _mm_sub_ps(fours, comb2));
    const __m128 m_shuf1  = _mm_shuffle_ps(mins, mins, _MM_SHUFFLE(0,0,0,3));
    const __m128 m_shuf2  = _mm_shuffle_ps(mins, mins, _MM_SHUFFLE(0,0,0,2));
    const __m128 res      = _mm_sqrt_ss( _mm_add_ss( _mm_add_ss(mins, m_shuf1), m_shuf2 ) );
    float result;
    _mm_store_ss(&result, res);
    return result;
#endif
}

template <>
double Quaternion3SIMD(const double* pVect1, const double* pVect2, size_t qty) {
    return Quaternion3Standard(pVect1, pVect2, qty);
}

template float  Quaternion3SIMD<float>(const float* pVect1, const float* pVect2, size_t qty);
template double Quaternion3SIMD<double>(const double* pVect1, const double* pVect2, size_t qty);


/*
 * EuclQuat1-distance.
 */

template <class T>
T EuclQuat1Standard(const T *p1, const T *p2, size_t qty)
{
    //Assumes qty = 7
    T sum1 = 0;
    T sum2 = 0;

    //euclidean part
    for (size_t i = 0; i < 3; i++) {
      T diff1 = (p1[i]-p2[i]);
      sum1 += diff1 * diff1;
    }

    //quaternion part
    for (size_t i = 3; i < 7; i++) {
      T diff2 = (p1[i]-p2[i]);
      sum2 += diff2 * diff2;
    }
    if (sum2 > 2.0)
        sum2 = 4.0 - sum2;

    return sqrt(sum1 + sum2);
}

template  float EuclQuat1Standard<float>(const float *p1, const float *p2, size_t qty);

/*
 * On new architectures unaligned loads are almost as fast as aligned ones.
 * Ensuring that both pVect1 and pVect2 are similarly aligned could be hard.
 */

template <>
float EuclQuat1SIMD(const float* pVect1, const float* pVect2, size_t qty) {
#ifndef PORTABLE_SSE2
#pragma message WARN("EuclQuat1SIMD<float>: SSE2 is not available, defaulting to pure C++ implementation!")
    return EuclQuat1Standard(pVect1, pVect2, qty);
#else
    //euclidean part
    const __m128 v1       = _mm_loadu_ps(pVect1);      // last element needs to be ignored
    const __m128 v2       = _mm_loadu_ps(pVect2);      // last element needs to be ignored
    const __m128 diff1    = _mm_sub_ps(v1, v2);
    const __m128 sqr1     = _mm_mul_ps(diff1, diff1);
    const __m128 shuffle1 = _mm_shuffle_ps(sqr1, sqr1, _MM_SHUFFLE(3,0,2,1));
	const __m128 sum1     = _mm_add_ps(sqr1, shuffle1);
	const __m128 shuffle2 = _mm_shuffle_ps(sqr1, sqr1, _MM_SHUFFLE(3,1,0,2));
	const __m128 sum2     = _mm_add_ps(sum1, shuffle2); //contains the squared sum in all registers except 3

    //quaternion part
    const __m128 v3   = _mm_loadu_ps(pVect1+3);
    const __m128 v4   = _mm_loadu_ps(pVect2+3);
    const __m128 diff3= _mm_sub_ps(v3, v4);
    const __m128 sqr3 = _mm_mul_ps(diff3, diff3);
    const __m128 shuffle3 = _mm_shuffle_ps(sqr3, sqr3, _MM_SHUFFLE(1,0,3,2));
	const __m128 sum3 = _mm_add_ps(sqr3, shuffle3);
	const __m128 shuffle4 = _mm_shuffle_ps(sum3, sum3, _MM_SHUFFLE(2,3,0,1));
	const __m128 sum4 = _mm_add_ps(sum3, shuffle4);
    const __m128 fours = _mm_set_ss(4.0);
    const __m128 mins  = _mm_min_ss(sum4, _mm_sub_ps(fours, sum4)); //contains the squared quaternion sum in first register

    //combine both parts
    const __m128 res_comb   = _mm_sqrt_ss(_mm_add_ss(sum2, mins));
    float result;
    _mm_store_ss(&result, res_comb);
    return result;
#endif
}

template float  EuclQuat1SIMD<float>(const float* pVect1, const float* pVect2, size_t qty);


/*
 * EuclQuat2-distance.
 */

template <class T>
T EuclQuat2Standard(const T *p1, const T *p2, size_t qty)
{
    //Assumes qty = 14

    //euclidean part
    T sum_eucl = 0;
    for (size_t i = 0; i < 3; i++) {
      T diff1   = (p1[i]-p2[i]);
      T diff2   = (p1[i+7]-p2[i+7]);
      sum_eucl += diff1 * diff1 + diff2 * diff2;
    }

    //quaternion part
    T sum_quat1 = 0;
    T sum_quat2 = 0;
    for (size_t i = 3; i < 7; i++) {
      T diff1 = (p1[i]-p2[i]);
      sum_quat1 += diff1 * diff1;
      T diff2 = (p1[i+7]-p2[i+7]);
      sum_quat2 += diff2 * diff2;
    }
    if (sum_quat1 > 2.0)
        sum_quat1 = 4.0 - sum_quat1;
    if (sum_quat2 > 2.0)
        sum_quat2 = 4.0 - sum_quat2;

    return sqrt(sum_eucl + sum_quat1 + sum_quat2);
}

template  float EuclQuat2Standard<float>(const float *p1, const float *p2, size_t qty);

/*
 * On new architectures unaligned loads are almost as fast as aligned ones.
 * Ensuring that both pVect1 and pVect2 are similarly aligned could be hard.
 */

template <>
float EuclQuat2SIMD(const float* pVect1, const float* pVect2, size_t qty) {
#ifndef PORTABLE_SSE2
#pragma message WARN("EuclQuat2SIMD<float>: SSE2 is not available, defaulting to pure C++ implementation!")
    return EuclQuat2Standard(pVect1, pVect2, qty);

#else
    //define vectors
    const __m128 v1e1   = _mm_set_ps(pVect1[0], pVect1[1], pVect1[2], 0.0f); //not very efficient...
    const __m128 v2e1   = _mm_set_ps(pVect2[0], pVect2[1], pVect2[2], 0.0f); //not very efficient...
    const __m128 v1e2   = _mm_set_ps(pVect1[7], pVect1[8], pVect1[9], 0.0f); //not very efficient...
    const __m128 v2e2   = _mm_set_ps(pVect2[7], pVect2[8], pVect2[9], 0.0f); //not very efficient...
    const __m128 v1q1   = _mm_loadu_ps(pVect1+3);
    const __m128 v2q1   = _mm_loadu_ps(pVect2+3);
    const __m128 v1q2   = _mm_loadu_ps(pVect1+10);
    const __m128 v2q2   = _mm_loadu_ps(pVect2+10);
    const __m128 fours = _mm_set_ss(4.0);

    //euclidean part
    const __m128 diff_e1  = _mm_sub_ps(v1e1, v2e1);
    const __m128 sqr_e1   = _mm_mul_ps(diff_e1, diff_e1);
    const __m128 shuf_e11 = _mm_shuffle_ps(sqr_e1, sqr_e1, _MM_SHUFFLE(1,0,3,2));
	const __m128 sum_e11  = _mm_add_ps(sqr_e1, shuf_e11);
	const __m128 shuf_e12 = _mm_shuffle_ps(sum_e11, sum_e11, _MM_SHUFFLE(2,3,0,1));
	const __m128 sum_e12  = _mm_add_ps(sum_e11, shuf_e12); //contains the squared sum in all registers

    const __m128 diff_e2  = _mm_sub_ps(v1e2, v2e2);
    const __m128 sqr_e2   = _mm_mul_ps(diff_e2, diff_e2);
    const __m128 shuf_e21 = _mm_shuffle_ps(sqr_e2, sqr_e2, _MM_SHUFFLE(1,0,3,2));
	const __m128 sum_e21  = _mm_add_ps(sqr_e2, shuf_e21);
	const __m128 shuf_e22 = _mm_shuffle_ps(sum_e21, sum_e21, _MM_SHUFFLE(2,3,0,1));
	const __m128 sum_e22  = _mm_add_ps(sum_e21, shuf_e22); //contains the squared sum in all registers

    //quaternion part
    const __m128 diff_q1  = _mm_sub_ps(v1q1, v2q1);
    const __m128 sqr_q1   = _mm_mul_ps(diff_q1, diff_q1);
    const __m128 shuf_q11 = _mm_shuffle_ps(sqr_q1, sqr_q1, _MM_SHUFFLE(1,0,3,2));
	const __m128 sum_q11  = _mm_add_ps(sqr_q1, shuf_q11);
	const __m128 shuf_q12 = _mm_shuffle_ps(sum_q11, sum_q11, _MM_SHUFFLE(2,3,0,1));
	const __m128 sum_q12  = _mm_add_ps(sum_q11, shuf_q12);
    const __m128 mins_q1  = _mm_min_ss(sum_q12, _mm_sub_ps(fours, sum_q12)); //contains the squared quaternion sum in first register

    const __m128 diff_q2  = _mm_sub_ps(v1q2, v2q2);
    const __m128 sqr_q2   = _mm_mul_ps(diff_q2, diff_q2);
    const __m128 shuf_q21 = _mm_shuffle_ps(sqr_q2, sqr_q2, _MM_SHUFFLE(1,0,3,2));
	const __m128 sum_q21  = _mm_add_ps(sqr_q2, shuf_q21);
	const __m128 shuf_q22 = _mm_shuffle_ps(sum_q21, sum_q21, _MM_SHUFFLE(2,3,0,1));
	const __m128 sum_q22  = _mm_add_ps(sum_q21, shuf_q22);
    const __m128 mins_q2  = _mm_min_ss(sum_q22, _mm_sub_ps(fours, sum_q22)); //contains the squared quaternion sum in first register

    //combine both parts
    const __m128 summ     = _mm_add_ss( _mm_add_ss(sum_e12, sum_e22), _mm_add_ss(mins_q1, mins_q2) );
    const __m128 res_comb = _mm_sqrt_ss(summ);
    float result;
    _mm_store_ss(&result, res_comb);
    return result;
#endif
}

template float  EuclQuat2SIMD<float>(const float* pVect1, const float* pVect2, size_t qty);


/*
 * QuaternionDP1-distance.
 * This is not a proper metric, because it does NOT fulfill the triangle inequality.
 */

template <class T>
T QuaternionDP1Standard(const T *p1, const T *p2, size_t qty)
{
    T sum = 0.0;

    for (size_t i = 0; i < qty; i++) {
      sum += p1[i] * p2[i];
    }

    return 1.0 - fabs(sum);
}

template  float QuaternionDP1Standard<float>(const float *p1, const float *p2, size_t qty);

template <>
float QuaternionDP1SIMD(const float* pVect1, const float* pVect2, size_t qty) {
#ifndef PORTABLE_SSE2
#pragma message WARN("QuaternionDP1SIMD<float>: SSE2 is not available, defaulting to pure C++ implementation!")
    return QuaternionDP1Standard(pVect1, pVect2, qty);
#else
    const __m128 v1         = _mm_loadu_ps(pVect1);                              // [ v1D v1C | v1B v1A ]
    const __m128 v2         = _mm_loadu_ps(pVect2);                              // [ v2D v2C | v2B v2A ]
    const __m128 mul        = _mm_mul_ps(v1, v2);                                // [ v1D*v2D v1C*v2C | v1B*v2B v1A*v2A ]

    // horizontal sum
    const __m128 shuffle1   = _mm_shuffle_ps(mul, mul, _MM_SHUFFLE(1,0,3,2));
	const __m128 sum1       = _mm_add_ps(mul, shuffle1);
	const __m128 shuffle2   = _mm_shuffle_ps(sum1, sum1, _MM_SHUFFLE(2,3,0,1));
	const __m128 sum2       = _mm_add_ps(sum1, shuffle2);

    // 1 - abs(x)
    float res;
    _mm_store_ss(&res, sum2);
    return 1 - fabs(res);
#endif
}

template float  QuaternionDP1SIMD<float>(const float* pVect1, const float* pVect2, size_t qty);


/*
 * Quaternion2max-distance.
 */

template <class T>
T Quaternion2maxStandard(const T *p1, const T *p2, size_t qty)
{
    T sum1 = 0;
    T sum2 = 0;

    for (size_t i = 0; i < 4; i++) {
      T diff1 = (p1[i]-p2[i]);
      sum1 += diff1 * diff1;

      T diff2 = (p1[i+4]-p2[i+4]);
      sum2 += diff2 * diff2;
    }

    if (sum1 > 2.0)
        sum1 = 4.0 - sum1;
    if (sum2 > 2.0)
        sum2 = 4.0 - sum2;

    return sqrt(max(sum1, sum2));
}

template  float Quaternion2maxStandard<float>(const float *p1, const float *p2, size_t qty);


template <>
float Quaternion2maxSIMD(const float* pVect1, const float* pVect2, size_t qty) {
#ifndef PORTABLE_SSE2
#pragma message WARN("Quaternion2maxSIMD<float>: SSE2 is not available, defaulting to pure C++ implementation!")
    return Quaternion2maxStandard(pVect1, pVect2, qty);

#else
    const __m128 v1       = _mm_loadu_ps(pVect1); pVect1 += 4;
    const __m128 v2       = _mm_loadu_ps(pVect2); pVect2 += 4;
    const __m128 diff1    = _mm_sub_ps(v1, v2);
    const __m128 sqr1     = _mm_mul_ps(diff1, diff1);
    const __m128 shuffle1 = _mm_shuffle_ps(sqr1, sqr1, _MM_SHUFFLE(1,0,3,2));
	const __m128 sum1     = _mm_add_ps(sqr1, shuffle1);
	const __m128 shuffle2 = _mm_shuffle_ps(sum1, sum1, _MM_SHUFFLE(2,3,0,1));
	const __m128 sum2     = _mm_add_ps(sum1, shuffle2);

    const __m128 v3       = _mm_loadu_ps(pVect1); //pVect1 += 4;
    const __m128 v4       = _mm_loadu_ps(pVect2); //pVect2 += 4;
    const __m128 diff3    = _mm_sub_ps(v3, v4);
    const __m128 sqr3     = _mm_mul_ps(diff3, diff3);
    const __m128 shuffle3 = _mm_shuffle_ps(sqr3, sqr3, _MM_SHUFFLE(1,0,3,2));
	const __m128 sum3     = _mm_add_ps(sqr3, shuffle3);
	const __m128 shuffle4 = _mm_shuffle_ps(sum3, sum3, _MM_SHUFFLE(2,3,0,1));
	const __m128 sum4     = _mm_add_ps(sum3, shuffle4);

    const __m128 fours    = _mm_set1_ps(4.0);
    const __m128 comb     = _mm_unpacklo_ps(sum2, sum4);
    const __m128 mins     = _mm_min_ps(comb, _mm_sub_ps(fours, comb));
    const __m128 mins_shuf= _mm_shuffle_ps(mins, mins, _MM_SHUFFLE(0,0,0,1));
    const __m128 res      = _mm_sqrt_ss(_mm_max_ss(mins, mins_shuf));
    float result;
    _mm_store_ss(&result, res);
    return result;
#endif
}

template float  Quaternion2maxSIMD<float>(const float* pVect1, const float* pVect2, size_t qty);


/*
 * Eucl1Quat2-distance.
 */

template <class T>
T Eucl1Quat2Standard(const T *p1, const T *p2, size_t qty)
{
    //Assumes qty = 11
    T sum1 = 0;
    T sum2 = 0;
    T sum3 = 0;

    //euclidean part
    for (size_t i = 0; i < 3; i++) {
      T diff1 = (p1[i]-p2[i]);
      sum1 += diff1 * diff1;
    }

    //quaternion part
    for (size_t i = 3; i < 7; i++) {
      T diff2 = (p1[i]-p2[i]);
      sum2 += diff2 * diff2;
      T diff3 = (p1[i+4]-p2[i+4]);
      sum3 += diff3 * diff3;
    }
    if (sum2 > 2.0)
        sum2 = 4.0 - sum2;
    if (sum3 > 2.0)
        sum3 = 4.0 - sum3;

    return sqrt(sum1 + sum2 + sum3);
}

template  float Eucl1Quat2Standard<float>(const float *p1, const float *p2, size_t qty);

/*
 * On new architectures unaligned loads are almost as fast as aligned ones.
 * Ensuring that both pVect1 and pVect2 are similarly aligned could be hard.
 */

template <>
float Eucl1Quat2SIMD(const float* pVect1, const float* pVect2, size_t qty) {
#ifndef PORTABLE_SSE2
#pragma message WARN("Eucl1Quat2SIMD<float>: SSE2 is not available, defaulting to pure C++ implementation!")
    return Eucl1Quat2Standard(pVect1, pVect2, qty);
#else
    //euclidean part
    const __m128 v1       = _mm_loadu_ps(pVect1);      // last element needs to be ignored
    const __m128 v2       = _mm_loadu_ps(pVect2);      // last element needs to be ignored
    const __m128 diff1    = _mm_sub_ps(v1, v2);
    const __m128 sqr1     = _mm_mul_ps(diff1, diff1);
    const __m128 shuffle1 = _mm_shuffle_ps(sqr1, sqr1, _MM_SHUFFLE(3,0,2,1));
	const __m128 sum1     = _mm_add_ps(sqr1, shuffle1);
	const __m128 shuffle2 = _mm_shuffle_ps(sqr1, sqr1, _MM_SHUFFLE(3,1,0,2));
	const __m128 sum2     = _mm_add_ps(sum1, shuffle2); //contains the squared sum in all registers except 3

    //quaternion part
    const __m128 vq1   = _mm_loadu_ps(pVect1+3);
    const __m128 vq2   = _mm_loadu_ps(pVect2+3);
    const __m128 diffq = _mm_sub_ps(vq1, vq2);
    const __m128 sqrq  = _mm_mul_ps(diffq, diffq);
    const __m128 shuffleq1 = _mm_shuffle_ps(sqrq, sqrq, _MM_SHUFFLE(1,0,3,2));
	const __m128 sumq1 = _mm_add_ps(sqrq, shuffleq1);
	const __m128 shuffleq2 = _mm_shuffle_ps(sumq1, sumq1, _MM_SHUFFLE(2,3,0,1));
	const __m128 sumq2 = _mm_add_ps(sumq1, shuffleq2);
    const __m128 fours = _mm_set_ss(4.0);
    const __m128 minsq = _mm_min_ss(sumq2, _mm_sub_ps(fours, sumq2)); //contains the squared quaternion sum in first register

    const __m128 vp1   = _mm_loadu_ps(pVect1+7);
    const __m128 vp2   = _mm_loadu_ps(pVect2+7);
    const __m128 diffp = _mm_sub_ps(vp1, vp2);
    const __m128 sqrp  = _mm_mul_ps(diffp, diffp);
    const __m128 shufflep1 = _mm_shuffle_ps(sqrp, sqrp, _MM_SHUFFLE(1,0,3,2));
	const __m128 sump1 = _mm_add_ps(sqrp, shufflep1);
	const __m128 shufflep2 = _mm_shuffle_ps(sump1, sump1, _MM_SHUFFLE(2,3,0,1));
	const __m128 sump2 = _mm_add_ps(sump1, shufflep2);
    const __m128 minsp = _mm_min_ss(sump2, _mm_sub_ps(fours, sump2)); //contains the squared quaternion sum in first register

    //combine all parts
    const __m128 res_comb   = _mm_sqrt_ss(_mm_add_ss(_mm_add_ss(sum2, minsq), minsp));
    float result;
    _mm_store_ss(&result, res_comb);
    return result;
#endif
}

template float  Eucl1Quat2SIMD<float>(const float* pVect1, const float* pVect2, size_t qty);


/*
 * Eucl2Quat1-distance.
 */

template <class T>
T Eucl2Quat1Standard(const T *p1, const T *p2, size_t qty)
{
    //Assumes qty = 10
    T sum1 = 0;
    T sum2 = 0;

    //euclidean part
    for (size_t i = 0; i < 3; i++) {
      T diff1 = (p1[i]-p2[i]);
      sum1 += diff1 * diff1;
      diff1 = (p1[i+3]-p2[i+3]);
      sum1 += diff1 * diff1;
    }

    //quaternion part
    for (size_t i = 6; i < 10; i++) {
      T diff2 = (p1[i]-p2[i]);
      sum2 += diff2 * diff2;
    }
    if (sum2 > 2.0)
        sum2 = 4.0 - sum2;

    return sqrt(sum1 + sum2);
}

template  float Eucl2Quat1Standard<float>(const float *p1, const float *p2, size_t qty);

/*
 * On new architectures unaligned loads are almost as fast as aligned ones.
 * Ensuring that both pVect1 and pVect2 are similarly aligned could be hard.
 */

template <>
float Eucl2Quat1SIMD(const float* pVect1, const float* pVect2, size_t qty) {
#ifndef PORTABLE_SSE2
#pragma message WARN("Eucl2Quat1SIMD<float>: SSE2 is not available, defaulting to pure C++ implementation!")
    return Eucl2Quat1Standard(pVect1, pVect2, qty);
#else
    //euclidean part
    const __m128 v1       = _mm_loadu_ps(pVect1);      // last element needs to be ignored
    const __m128 v2       = _mm_loadu_ps(pVect2);      // last element needs to be ignored
    const __m128 diff1    = _mm_sub_ps(v1, v2);
    const __m128 sqr1     = _mm_mul_ps(diff1, diff1);
    const __m128 shuffle1 = _mm_shuffle_ps(sqr1, sqr1, _MM_SHUFFLE(3,0,2,1));
	const __m128 sum1     = _mm_add_ps(sqr1, shuffle1);
	const __m128 shuffle2 = _mm_shuffle_ps(sqr1, sqr1, _MM_SHUFFLE(3,1,0,2));
	const __m128 sum2     = _mm_add_ps(sum1, shuffle2); //contains the squared sum in all registers except 3

    const __m128 v3       = _mm_loadu_ps(pVect1+3);      // last element needs to be ignored
    const __m128 v4       = _mm_loadu_ps(pVect2+3);      // last element needs to be ignored
    const __m128 diff2    = _mm_sub_ps(v3, v4);
    const __m128 sqr2     = _mm_mul_ps(diff2, diff2);
    const __m128 shuffle3 = _mm_shuffle_ps(sqr2, sqr2, _MM_SHUFFLE(3,0,2,1));
	const __m128 sum3     = _mm_add_ps(sqr2, shuffle3);
	const __m128 shuffle4 = _mm_shuffle_ps(sqr2, sqr2, _MM_SHUFFLE(3,1,0,2));
	const __m128 sum2B    = _mm_add_ps(sum3, shuffle4); //contains the squared sum in all registers except 3

    //quaternion part
    const __m128 v3q   = _mm_loadu_ps(pVect1+6);
    const __m128 v4q   = _mm_loadu_ps(pVect2+6);
    const __m128 diff3q= _mm_sub_ps(v3q, v4q);
    const __m128 sqr3q = _mm_mul_ps(diff3q, diff3q);
    const __m128 shuffle3q = _mm_shuffle_ps(sqr3q, sqr3q, _MM_SHUFFLE(1,0,3,2));
	const __m128 sum3q = _mm_add_ps(sqr3q, shuffle3q);
	const __m128 shuffle4q = _mm_shuffle_ps(sum3q, sum3q, _MM_SHUFFLE(2,3,0,1));
	const __m128 sum4q = _mm_add_ps(sum3q, shuffle4q);
    const __m128 fours = _mm_set_ss(4.0);
    const __m128 mins  = _mm_min_ss(sum4q, _mm_sub_ps(fours, sum4q)); //contains the squared quaternion sum in first register

    //combine all parts
    const __m128 res_comb   = _mm_sqrt_ss(_mm_add_ss(_mm_add_ss(sum2, sum2B), mins));
    float result;
    _mm_store_ss(&result, res_comb);
    return result;
#endif
}

template float  Eucl2Quat1SIMD<float>(const float* pVect1, const float* pVect2, size_t qty);

} // closing namespace
