/**
 * Non-metric Space Library
 *
 * Authors: Bilegsaikhan Naidan (https://github.com/bileg), Leonid Boytsov (http://boytsov.info).
 * With contributions from Lawrence Cayton (http://lcayton.com/) and others.
 *
 * For the complete list of contributors and further details see:
 * https://github.com/searchivarius/NonMetricSpaceLib
 *
 * Copyright (c) 2014
 *
 * This code is released under the
 * Apache License Version 2.0 http://www.apache.org/licenses/.
 *
 */

#ifndef FACTORY_SPACE_QUATERNION_H
#define FACTORY_SPACE_QUATERNION_H

#include <space/space_quaternion.h>

namespace similarity {

/*
 * Creating functions.
 */

template <typename dist_t>
Space<dist_t>* CreateQuaternion1Distance(const AnyParams& /* ignoring params */) {
  return new SpaceQuaternion1Distance<dist_t>();
}


template <typename dist_t>
Space<dist_t>* CreateQuaternion2Distance(const AnyParams& /* ignoring params */) {
  return new SpaceQuaternion2Distance<dist_t>();
}


template <typename dist_t>
Space<dist_t>* CreateQuaternion3Distance(const AnyParams& /* ignoring params */) {
  return new SpaceQuaternion3Distance<dist_t>();
}

template <typename dist_t>
Space<dist_t>* CreateEuclQuat1Distance(const AnyParams& /* ignoring params */) {
  return new SpaceEuclQuat1Distance<dist_t>();
}

template <typename dist_t>
Space<dist_t>* CreateEucl2Quat1Distance(const AnyParams& /* ignoring params */) {
  return new SpaceEucl2Quat1Distance<dist_t>();
}

template <typename dist_t>
Space<dist_t>* CreateEucl1Quat2Distance(const AnyParams& /* ignoring params */) {
  return new SpaceEucl1Quat2Distance<dist_t>();
}

template <typename dist_t>
Space<dist_t>* CreateEuclQuat2Distance(const AnyParams& /* ignoring params */) {
  return new SpaceEuclQuat2Distance<dist_t>();
}


template <typename dist_t>
Space<dist_t>* CreateQuaternionDP1Distance(const AnyParams& /* ignoring params */) {
  return new SpaceQuaternionDP1Distance<dist_t>();
}


template <typename dist_t>
Space<dist_t>* CreateQuaternion2maxDistance(const AnyParams& /* ignoring params */) {
  return new SpaceQuaternion2maxDistance<dist_t>();
}

/*
 * End of creating functions.
 */

}

#endif
