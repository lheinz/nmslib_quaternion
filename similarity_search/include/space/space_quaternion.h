/**
 * Non-metric Space Library
 *
 * Main developers: Bilegsaikhan Naidan, Leonid Boytsov, Yury Malkov, Ben Frederickson, David Novak
 *
 * For the complete list of contributors and further details see:
 * https://github.com/searchivarius/NonMetricSpaceLib
 *
 * Copyright (c) 2013-2018
 *
 * This code is released under the
 * Apache License Version 2.0 http://www.apache.org/licenses/.
 *
 */
#ifndef _SPACE_QUATERNION_H_
#define _SPACE_QUATERNION_H_

#include <string>
#include <limits>
#include <map>
#include <stdexcept>

#include <string.h>
#include "global.h"
#include "object.h"
#include "utils.h"
#include "space.h"
#include "space_vector.h"
#include "distcomp.h"

#define SPACE_QUATERNION1     "quaternion1"
#define SPACE_QUATERNION2     "quaternion2"
#define SPACE_QUATERNION3     "quaternion3"

#define SPACE_EUCLQUAT1       "euclquat1"
#define SPACE_EUCL2QUAT1      "eucl2quat1"
#define SPACE_EUCL1QUAT2      "eucl1quat2"
#define SPACE_EUCLQUAT2       "euclquat2"

#define SPACE_QUATERNIONDP1   "quaternionDP1"

#define SPACE_QUATERNION2MAX  "quaternion2max"

namespace similarity {


template <typename dist_t>
class SpaceQuaternion1Distance : public VectorSpaceSimpleStorage<dist_t> {
public:
  SpaceQuaternion1Distance() {}
  virtual std::string StrDesc() const {
    return "Quaternion1Distance";
  }
protected:
  virtual dist_t HiddenDistance(const Object* obj1, const Object* obj2) const;
  DISABLE_COPY_AND_ASSIGN(SpaceQuaternion1Distance);
};


template <typename dist_t>
class SpaceQuaternion2Distance : public VectorSpaceSimpleStorage<dist_t> {
public:
  SpaceQuaternion2Distance() {}
  virtual std::string StrDesc() const {
    return "Quaternion2Distance";
  }
protected:
  virtual dist_t HiddenDistance(const Object* obj1, const Object* obj2) const;
  DISABLE_COPY_AND_ASSIGN(SpaceQuaternion2Distance);
};


template <typename dist_t>
class SpaceQuaternion3Distance : public VectorSpaceSimpleStorage<dist_t> {
public:
  SpaceQuaternion3Distance() {}
  virtual std::string StrDesc() const {
    return "Quaternion3Distance";
  }
protected:
  virtual dist_t HiddenDistance(const Object* obj1, const Object* obj2) const;
  DISABLE_COPY_AND_ASSIGN(SpaceQuaternion3Distance);
};


template <typename dist_t>
class SpaceEuclQuat1Distance : public VectorSpaceSimpleStorage<dist_t> {
public:
  SpaceEuclQuat1Distance() {}
  virtual std::string StrDesc() const {
    return "EuclQuat1Distance";
  }
protected:
  virtual dist_t HiddenDistance(const Object* obj1, const Object* obj2) const;
  DISABLE_COPY_AND_ASSIGN(SpaceEuclQuat1Distance);
};


template <typename dist_t>
class SpaceEucl2Quat1Distance : public VectorSpaceSimpleStorage<dist_t> {
public:
  SpaceEucl2Quat1Distance() {}
  virtual std::string StrDesc() const {
    return "Eucl2Quat1Distance";
  }
protected:
  virtual dist_t HiddenDistance(const Object* obj1, const Object* obj2) const;
  DISABLE_COPY_AND_ASSIGN(SpaceEucl2Quat1Distance);
};


template <typename dist_t>
class SpaceEucl1Quat2Distance : public VectorSpaceSimpleStorage<dist_t> {
public:
  SpaceEucl1Quat2Distance() {}
  virtual std::string StrDesc() const {
    return "Eucl1Quat2Distance";
  }
protected:
  virtual dist_t HiddenDistance(const Object* obj1, const Object* obj2) const;
  DISABLE_COPY_AND_ASSIGN(SpaceEucl1Quat2Distance);
};


template <typename dist_t>
class SpaceEuclQuat2Distance : public VectorSpaceSimpleStorage<dist_t> {
public:
  SpaceEuclQuat2Distance() {}
  virtual std::string StrDesc() const {
    return "EuclQuat2Distance";
  }
protected:
  virtual dist_t HiddenDistance(const Object* obj1, const Object* obj2) const;
  DISABLE_COPY_AND_ASSIGN(SpaceEuclQuat2Distance);
};

template <typename dist_t>
class SpaceQuaternionDP1Distance : public VectorSpaceSimpleStorage<dist_t> {
public:
  SpaceQuaternionDP1Distance() {}
  virtual std::string StrDesc() const {
    return "QuaternionDP1Distance";
  }
protected:
  virtual dist_t HiddenDistance(const Object* obj1, const Object* obj2) const;
  DISABLE_COPY_AND_ASSIGN(SpaceQuaternionDP1Distance);
};

template <typename dist_t>
class SpaceQuaternion2maxDistance : public VectorSpaceSimpleStorage<dist_t> {
public:
  SpaceQuaternion2maxDistance() {}
  virtual std::string StrDesc() const {
    return "Quaternion2maxDistance";
  }
protected:
  virtual dist_t HiddenDistance(const Object* obj1, const Object* obj2) const;
  DISABLE_COPY_AND_ASSIGN(SpaceQuaternion2maxDistance);
};

}  // namespace similarity

#endif
