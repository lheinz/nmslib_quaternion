import numpy as np
import nmslib
import time

def nearest_distances(X, k=2, metric='quaternion1'):
    '''
    X = array(N,M)
    N = number of points
    M = number of dimensions
    returns the distance to the kth nearest neighbor for every point in X
    '''

    index = nmslib.init(method='vptree', space=metric, 
                        data_type = nmslib.DataType.DENSE_VECTOR,
                        dtype = nmslib.DistType.FLOAT) #, space_params= {'bucketSize': 100})
    index.addDataPointBatch(X)
    index.createIndex(print_progress=False)
    #index.setQueryTimeParams({'maxLeavesToVisit': 2147483647})
    neighbors = index.knnQueryBatch(X, k=k, num_threads=1)
    return np.array([n[1][-1] for n in neighbors])

def testMetric(metric, samplefile):
    print 'Testing %s metric ...' %metric
    S = np.load('%s.npy' %samplefile)
    r_real = np.load('%s_NN_distances.npy' %samplefile)
    start = time.time()
    r = nearest_distances(S, k=2, metric=metric)
    stopp = time.time()
    if np.allclose(r,r_real, rtol=1e-5, atol=1e-5):
        print '... test passed!'
    else:
        print '... test failed!'
    print 'Elapsed time: %fs\n' %(stopp-start)
    return r, r_real


r, r_real = testMetric('quaternion1', 'quaternions')
r, r_real = testMetric('quaternion2', 'quaternions2')
r, r_real = testMetric('quaternion3', 'quaternions3')
r, r_real = testMetric('euclquat1', 'euclquats1')
r, r_real = testMetric('quaternion2max', 'quaternions2max')
r, r_real = testMetric('eucl2quat1', 'eucl2quat1')
r, r_real = testMetric('eucl1quat2', 'eucl1quat2')

#%%
#dmin = np.inf
#
#i = 47
#for s in S[i+1:]:
#    d1 = min(np.linalg.norm(s[0:4]-S[i][0:4]), (np.linalg.norm(s[0:4]+S[i][0:4])))
#    d2 = min(np.linalg.norm(s[4:]-S[i][4:]), (np.linalg.norm(s[4:]+S[i][4:])))
#    d = max(d1,d2)
#    if d < dmin:
#        dmin = d
#        
#print dmin
#print r[i]