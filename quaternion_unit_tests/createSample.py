import numpy as np

Nframes = 1000

def quaternionMultiplication(q,r):
    q0, q1, q2, q3 = q
    r0, r1, r2, r3 = r
    
    t0 = r0*q0 - r1*q1 - r2*q2 - r3*q3
    t1 = r0*q1 + r1*q0 - r2*q3 + r3*q2
    t2 = r0*q2 + r1*q3 + r2*q0 - r3*q1
    t3 = r0*q3 - r1*q2 + r2*q1 + r3*q0    
    return np.array([ t0, t1, t2, t3 ])

#%% Draw random, uniform samples
def drawRandomRotation():
    R = np.random.random(3)
    alpha, beta, gamma = 2*np.pi*R[0], np.arccos(2*R[1]-1), 2*np.pi*R[2]
    pricAxis = np.sin(alpha)*np.sin(beta), np.cos(alpha)*np.sin(beta), np.cos(beta)
    rotAxis  = np.array([pricAxis[1], -pricAxis[0], 0.0]) # pricAxis x (0,0,1)^T
    nRotAxis = np.linalg.norm(rotAxis)
    rotAxis  = rotAxis/nRotAxis
    #print np.arcsin(nRotAxis)
    theta   = np.arccos(np.dot(pricAxis,np.array([0,0,1.])))
    #print theta
    sintheta2= np.sin(theta/2.)
    q_axis   = np.array([ np.cos(theta/2.), rotAxis[0]*sintheta2, rotAxis[1]*sintheta2, rotAxis[2]*sintheta2 ])
    q_z      = np.array([ np.cos(gamma/2.), 0, 0, np.sin(gamma/2.) ])
    return quaternionMultiplication(q_axis, q_z)
    
#    while True:    
#        R = 2.*np.random.random(4) - 1.
#        nR = np.linalg.norm(R)
#        if nR <= 1.0:
#            return R/nR

def matrix2quaternion(R):
    q2 = np.zeros(4)
    q2[0] = np.sqrt( max(0, 1+R[0,0]+R[1,1]+R[2,2]) ) /2.
    q2[1] = np.sqrt( max(0, 1+R[0,0]-R[1,1]-R[2,2]) ) /2.
    q2[2] = np.sqrt( max(0, 1-R[0,0]+R[1,1]-R[2,2]) ) /2.
    q2[3] = np.sqrt( max(0, 1-R[0,0]-R[1,1]+R[2,2]) ) /2.
    q2[1] = np.sign(R[2,1]-R[1,2]) * np.abs(q2[1])
    q2[2] = np.sign(R[0,2]-R[2,0]) * np.abs(q2[2])
    q2[3] = np.sign(R[1,0]-R[0,1]) * np.abs(q2[3])
        
    return q2

def drawRandomRotation2(dim=3):
    """According to """
    Q,R = np.linalg.qr(np.random.normal(size=(dim,dim)))
    r = np.diag(R)
    r = r/np.abs(r)
    L = np.diag(r)
    res = np.dot(Q,L)
    if np.linalg.det(res) > 0:
        return matrix2quaternion(res)
    else:
        return drawRandomRotation2(dim)

def quaternion2euler((q0,q1,q2,q3)):
    phi = np.arctan2(2*(q0*q1+q2*q3), 1-2*(q1*q1+q2*q2))
    the = np.arcsin(2*(q0*q2+q3*q1))
    psi = np.arctan2(2*(q0*q3+q1*q2), 1-2*(q2*q2+q3*q3))
    return np.array([phi,the,psi])

def createUniformSample(Nframes, maxdistance=np.pi):
    #returns a sample of uniform quaternions which have a maximal distance to the neutral element (1,0,0,0).
    Traj = np.zeros((Nframes,4))
    TrajE =np.zeros((Nframes,3))
    for t in xrange(Nframes):
        ok = False
        while not ok:
            x = drawRandomRotation2()
            dist = 2*np.arccos(np.abs(x[0]))
            if dist < maxdistance:
                ok = True
        Traj[t] = x
        TrajE[t]= quaternion2euler(x)
    return Traj, TrajE

def uniformSampleEntropy(maxdistance=np.pi):
    return np.log(8*np.pi*(maxdistance - np.sin(maxdistance)))

def removeAmbiguity(Traj,metric='euclidean',ref=np.array([1.,0,0,0])):
    if metric=='euclidean':
        dist = lambda q: np.linalg.norm(ref-q)
    else:
        dist = lambda q: metric(ref,q)
        
    Nframes, d = Traj.shape
    Traj_new = np.zeros((Nframes,d))
    
    for t in xrange(Nframes):
        q = Traj[t]
        d1 = dist(q)
        d2 = dist(-q)
        if d1<=d2:
            Traj_new[t] = q
        else:
            Traj_new[t] = -q
    return Traj_new


#%% create Eucl1Quat2 sample
X = np.zeros((Nframes, 11))

for i in xrange(Nframes):
    X[i][0:3] = np.random.normal(size=3)
    X[i][3:7] = drawRandomRotation2()
    X[i][7:] = drawRandomRotation2()
    
def d(x,y):
    s1 = np.linalg.norm(x[0:3] - y[0:3])**2
    s2 = np.linalg.norm(x[3:7] - y[3:7])**2
    s3 = np.linalg.norm(x[3:7] + y[3:7])**2
    s4 = np.linalg.norm(x[7:] - y[7:])**2
    s5 = np.linalg.norm(x[7:] + y[7:])**2
    return np.sqrt(s1 + np.min([s2,s3]) + np.min([s4,s5]))

R = np.zeros(Nframes)
for i in xrange(Nframes):
    rc = np.inf
    for j in xrange(Nframes):
        if i != j:
            r = d(X[i], X[j])
            if r < rc:
                rc = r
    R[i] = rc
    
np.save('eucl1quat2_NN_distances.npy', R)
np.save('eucl1quat2.npy', X)
